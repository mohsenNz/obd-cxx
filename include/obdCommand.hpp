/*
 * @author Luke Krasner (luke@lukekrasner.com)
 *
 * @class ObdCommand
 * @brief Class to represent a single runnable OBD-II command
 * @details the key to this class is the fact that the constructor takes a
 * pointer to an ObdSerial object.  this allows for very tight memory
 * management and keeps code clean.  An Obd command is runnable, so getting the
 * result is as simple as calling run() assuming the proper functio is given.
 * muparser should handle any function you need for custom commands.
 */
#ifndef OBDCOMMAND_HPP
#define OBDCOMMAND_HPP

#include <functional>
#include "obdSerial.hpp"

using namespace std;

class ObdCommand
{
public:
  /*
   * @brief Constructor to initialize an ObdCommand
   * @param *serial Ptr to an existing ObdSerial object for communication
   * @param name Human readable name of command
   * @param cmd String of command to send
   * @param func Formual function to calculate readable output
   */
  template<typename Func>
  ObdCommand(ObdSerial *serial, string name, string description, string cmd, Func func)
  {
    _name = name;
    _description = description;
    _command = cmd;
    _mode = cmd[1];
    _pid = cmd.substr(2, 4);
    _obd = serial;
    _calculate = func;
    _bytesReterned = 0;
    for (int i = 0; i < 4; i++) {
      _vars[i] = 0;
    }
  };

  /*
   * @brief sends the command, parses the result and returns
   * @return the parsed result
   */
  double query();

  string name();

  string command();

  string pid();

  string mode();

  int a();

  int b();

  int c();

  int d();

  int bytesReturned();

private:
  string _name;

  string _description;

  string _command;

  function<double(int, int, int, int)> _calculate;

  ObdSerial *_obd;

  string _pid, _mode;

  int _vars[4];  // array of vars to hold response bytes

  int _bytesReterned;  // number of bytes that returned by query()
};

#endif
