obd-cxx
=======

A simple C++ OBD-II library.

Requirements
============
make

gcc

Installation
============
```shell
./configure
make
sudo make install
```
