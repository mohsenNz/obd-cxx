//ObdCommand.cpp

#include "obdCommand.hpp"

double ObdCommand::query()
{
  vector<int> vals = _obd->sendObdCommand(_command);
  _bytesReterned = vals.size();
  if(_bytesReterned == 0) {
    throw -1;
    return -1.0;
  }
  
  else {
    for (int i = 0; i < _bytesReterned; i++) {
      _vars[i] = vals[i];
    }

    if (_calculate)
      return _calculate(_vars[0], _vars[1], _vars[2], _vars[3]);
    else 
      return 0;
  }
}

string ObdCommand::name() { return _name; }

string ObdCommand::command() { return _command; }

string ObdCommand::pid() { return _pid; }

string ObdCommand::mode() { return _mode; }

int ObdCommand::a() { return _vars[0]; }

int ObdCommand::b() { return _vars[1]; }

int ObdCommand::c() { return _vars[2]; }

int ObdCommand::d() { return _vars[3]; }

int ObdCommand::bytesReturned() { return _bytesReterned; }

