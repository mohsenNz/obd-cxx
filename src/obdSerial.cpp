// ObdSerial.cpp

#include "obdSerial.hpp"


ObdSerial::ObdSerial(const char *port, int baudrate)
{
  fd = openPort(port, baudrate);

  if (fd != -1)
    setup();
}

ObdSerial::~ObdSerial()
{
  close(fd);
  cout << "closing" << endl;
}

int ObdSerial::openPort(const char *port, int baudrate)
{
  struct termios options;
  int fd;

  cout << "Opening com port: " << port << endl;

  fd = open(port, O_RDWR | O_NOCTTY | O_NDELAY);

  if(fd == -1) {
    cout << "Failed to connect!" << endl;
    return -1;
  } else {
    fcntl(fd, F_SETFL, 0);

    // Get the current options for the port
    tcgetattr(fd, &options);
  
    //very important stuff that I absolutely fully understand
    options.c_cflag |= (CLOCAL | CREAD);
    options.c_lflag &= !(ICANON | ECHO | ECHOE | ISIG);
    options.c_oflag &= !(OPOST);
    options.c_cc[VMIN] = 0;
    options.c_cc[VTIME] = 100;

    cfsetispeed(&options, baudrate);
    cfsetospeed(&options, baudrate);
    tcsetattr(fd, TCSAFLUSH, &options);
  }
  
  cout << "Connected!" << endl;
  return fd;
}

bool ObdSerial::setup()
{
  sendGenCommand("ATZ"); //reset
  sendGenCommand("ATE0"); //disable echo
  sendGenCommand("ATL0"); //disable line feeds
  sendGenCommand("ATS0"); //disable spaces
  sendGenCommand("ATSP0"); //auto mode

  return true;
}

string ObdSerial::sendGenCommand(string command, bool addReturn)
{
  //cout << "command: " << command << endl; // debug

  if(addReturn)
    command += "\r";
  
  int n = write(fd, command.c_str(), command.length());

  if (n < 0) {
    cerr << "write() failed!" << endl;
  }

  char buf[1];

  string response = "";

  while (read(fd, (void*) buf, 1) > 0) {
    response.append(buf);
    if (response.back() == '>')
      break;
  }

  //cout << response << endl; // debug

  return response;
}

vector<int> ObdSerial::sendObdCommand(string command)
{
  vector<int> res;
  string r = sendGenCommand(command);
  //cout << "r: " << r << endl; // debug
  if (r.find("UNABLE TO CONNECT") != string::npos) {
    cout << "ECU connection failed!" << endl;
    return vector<int>();
  }

  if (r.find("SEARCHING") != string::npos) {
    r.erase(0 , r.find_first_of("\n\r") + 1); // delete command info
  }
  r.pop_back(); // delete prompt char (">")
  r.pop_back(); // delete free space

  //cout << "r after: " << r << endl; // debug
  
  for(unsigned int i = 4, j = 0; i < r.length() - 1; i+=2, j++) {
    try {
      res.push_back(stoi(r.substr(i,2), nullptr, 16));
    } catch(...) {
      cout << "Unable convert integer!" << endl;
      return vector<int>();
    }
  }

  return res;
}
